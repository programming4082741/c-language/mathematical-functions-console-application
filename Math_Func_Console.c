#include<stdio.h>
#include<math.h>

double va_absolue(double x){
	if(x>=0)
		return x;
	return -x;
}

unsigned int sn_arith(unsigned int n){
	if(n==0)
		return 0;
	return ((n+1)*n)/2;
}

unsigned int sn_carre(unsigned int n)
{
	if(n==0)
		return 0;
	if(n==1)
		return 1;
	return (((n+1)*n)*(2*n+1))/6;
}

unsigned int fact(unsigned int k)
{
	if(k==0 || k==1)
		return 1;
	return k*fact(k-1);
}

int pgcd(unsigned a,unsigned b){
	if(b==0)
		return a;
	return pgcd(b,a%b);
}

int premier(int p){
	int i;
	for(i=2;i<=sqrt(p);i++){
		if(p%i!=0)
			return 0;
	}
	return 1;
}

int nbre_amis(unsigned a, unsigned b){
	int i,sa=0,sb=0;
	for(i=2;i<=a/2;i++)
		if(a%i==0)
			sa=sa+i;
	for(i=2;i<=b/2;i++)
		if(b%i==0)
			sb=sb+i;
	if(sb==a && sa==b)
		return 1;
	return 0;
}

int main(){
	int c;
	unsigned n;
	
	printf("\nEntrez la valeur convenable pour les options ci-dessous:\n\n\t1-Somme d\'ordre n des entiers positifs \t\t2-Somme d\'ordre n de entiers positifs pairs\n\n\t3-Somme des carres d\'ordre n \t\t\t4-Le factorielle de n\n\n\t5-Calcul de pgcd \t\t\t\t6-Solution d\'equation de second degres \n\n\t7-Savoir si un entier est premier \t\t8-Savoir si deux entiers sont amis \n\n\t9-Valeur absolue\n\n");
	do{
		do{
			scanf("%d",&c);
			if(c<1 || c>9){
				printf("\aErreur: Les valeurs acceptable sont \"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\".\n");
			}
		}while(c<1 || c>9);
		if(c==1){
		printf("Saisir un entier positif.\n");
		scanf("%u",&n);
		printf("La somme d\'ordre %u des entiers positifs est :\n\t Sn(%u)=%u",n,n,sn_arith(n));
		}
	
		if(c==2){
			printf("Saisir un entier positif.\n");
			scanf("%u",&n);
			if(n%2==0){
				printf("La somme d\'ordre %u des entiers positifs pair est :\n\t Sn(%u)=%u",n,n,sn_arith(n/2)*2);	
			}
			else{
				printf("La somme d\'ordre %u des entiers positifs pair est :\n\t Sn(%u)=%u",n,n,sn_arith((n-1)/2)*2);
			}
		}
		if(c==3){
			printf("Saisir un entier positif.\n");
			scanf("%u",&n);
			printf("Somme des carres d\'ordre %u est :\n\t Sn(%u)=%u",n,n,sn_carre(n));
		}
		if(c==4){
			printf("Saisir un entier positif.\n");
			scanf("%u",&n);
			printf("Le factorielle de %u :\n\t %u!=%u",n,n,fact(n));
		}
		if(c==5){
			unsigned m,n;
			printf("Saisir un entier m.\n");
			scanf("%u",&m);
			printf("Saisir un entier n.\n");
			scanf("%u",&n);
			printf("le pgcd(%u,%u) = %u",m,n,pgcd(m,n));
		}
		if(c==6){			
			float a,b,c3,D;
			float x;	
			printf("Saisir \"a\" pour ax*x+bx+c=0 :\n");
			scanf("%f",&a);
			printf("Saisir \"b\" pour ax*x+bx+c=0 :\n");
			scanf("%f",&b);
			printf("Saisir \"c\" pour ax*x+bx+c=0 :\n");
			scanf("%f",&c3);
			D=pow(b,2)-4*a*c3;
			if(a!=0){
				if(b!=0){
					if(c3!=0){
						if(D>=0){
							x=(-b+sqrt(D))/2*a;
							printf("Les solutions dans \"R\" sont :\n","	", x,"	", -x-(b/a));
						}
						else{
							printf("Les solutions dans \"C\" sont :\n	%f + i %f		%f - i %f", -b/(2*a), sqrt(-D)/(2*a), -b/(2*a), sqrt(-D)/(2*a));
						}
					}
					else{
						printf("Les solutions sont :\n	0	%f",-b/a);
					}
				}
				else{
					if(c3!=0){
						if(D>=0){
							printf("Les solutions dans \"R\" sont :\n","	", x=sqrt(D)/(2*a), "		", -x-(b/a));
						}
						else{
							printf("les solutions dans \"C\" sont :\n	i%f		-i%f", sqrt(-D)/(2*a));
						}
					}
					else{
						printf("La solutions unique dans \"R\" est :\n","	0");
					}
				}	
			}
			else{
				if(b!=0){
					if(c3!=0){
						printf("La solution unique dans \"R\" est :\n	%f",-c3/b);
					}
					else{
						printf("La solutions unique dans\"R\" est :\n	0");
					}
				}
				else{
					if(c3!=0){
						printf("%fx + %f n\'admet pas de solution", b, c3);
					}
					else{
						printf("	\"R\" est l\'ensemble de solutions");
					}
				}
			}
		}
		if(c==7){
			unsigned p;
			printf("Saisir un entier positif.\nRemarque: Un nombre negatif sera converti en positif.\n");
			scanf("%d",&p);
			if(premier(p)==1){
				printf("%d est premier.",p);
			}
			else{
				printf("%d n\'est pas premier.",p);
			}
		}
		if(c==8){
			unsigned w,x;
			printf("Saisir deux entiers positifs. \tRemarque: Les nombre negatif seront convertis en positifs.\n");
			scanf("%d",&w);
			printf("\n");
			scanf("%d",&x);
			if(nbre_amis(w,x)==1){
				printf("%d et %d saisis sont amis.",w,x);
			}
			else{
				printf("%d et %d ne sont pas amis.",w,x);
			}
		}
		if(c==9){
			double z;
			printf("Saisir un reel.\n");
			scanf("%lf",&z);
			printf("\t[%lf]=%lf",z,va_absolue(z));
		}
	printf("\n\nSaisir a nouveau une option.\n\n");
	}while(c>1 || c<=9);
}
