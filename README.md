# Mathematical Functions Console Application

This C program provides a user-friendly interface for performing various mathematical calculations and operations. Developed with a focus on mathematical functionality, this application allows users to calculate sums, factorials, greatest common divisors, check for prime numbers, and more.

## Project Overview

### Mathematical Functions
This C program includes functions for various mathematical operations:

1. **Absolute Value**: Calculate the absolute value of a real number.

2. **Sum of Positive Integers**: Calculate the sum of positive integers up to a specified value (arithmetic sum).

3. **Sum of Positive Even Integers**: Calculate the sum of positive even integers up to a specified value.

4. **Sum of Squares**: Calculate the sum of the squares of integers up to a specified value.

5. **Factorial**: Calculate the factorial of a positive integer.

6. **Greatest Common Divisor (GCD)**: Calculate the greatest common divisor of two positive integers.

7. **Prime Number Check**: Determine whether a given integer is a prime number.

8. **Amicable Numbers Check**: Check if two positive integers are amicable numbers.

### User Interaction
Users can select a mathematical operation by entering the corresponding option number. The program guides users through the process and provides results for their chosen operations.

### Input and Output
Users input values or numbers as required for each operation, and the program displays the results.

## Project Goals

- Provide a menu-driven interface for mathematical operations.
- Enable users to perform mathematical calculations and checks easily.
- Offer a wide range of mathematical functions, including absolute value, sums, factorials, GCD, and more.